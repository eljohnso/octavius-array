% Specify the directory containing the .mat files
folder = 'Day 2/';

% Get a list of all .mat files in the folder
matFiles = dir(fullfile(folder, '*.mat'));

% Loop through each .mat file
for k = 1:length(matFiles)
    % Load the .mat file
    matFileName = fullfile(folder, matFiles(k).name);
    load(matFileName);

    % Check if Startdate, Enddate, and SRSmatrix exist in the file
    if exist('Startdate', 'var') && exist('Enddate', 'var') && exist('SRSmatrix', 'var')
        % Convert Startdate and Enddate to vectors
        startdateVec = datevec(Startdate);
        enddateVec = datevec(Enddate);

        % Prepare the HDF5 file name
        [~, baseFileName, ~] = fileparts(matFileName);
        hdf5FileName = fullfile(folder, strcat(baseFileName, '.h5'));

        % Save data in HDF5 format
        h5create(hdf5FileName, '/SRSmatrix', size(SRSmatrix));
        h5write(hdf5FileName, '/SRSmatrix', SRSmatrix);
        h5writeatt(hdf5FileName, '/SRSmatrix', 'Startdate', startdateVec);
        h5writeatt(hdf5FileName, '/SRSmatrix', 'Enddate', enddateVec);
    else
        fprintf('Required variables not found in %s\n', matFileName);
    end
end
