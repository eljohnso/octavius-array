# octavius-array

Data taken on the octavius array during the HEARTS/CHIMERA october 2023 campaign.

# Octavius Array Campaign Image

<img src="octavius_location_pictures/octavius_array_on_montrac.png" width="400" />

# Location of the Array in CHARM

<img src="octavius_location_pictures/diagram_position.png" width="700" />


# Measurements information

Andreas Pflaum has now prepared a Matlab file for every measurement which contains the measurement data of the 1600SRS array and a startdate variable as well as an enddate variable. The dose values are NOT corrected for air pressure, temperature and dose response yet, and there is no interpolation or post processing involved, only raw data.

We have used amplification factors for both arrays, so that the measurements for the 1600SRS array show dose values 20 times higher than the real values, the measurements of the 1600XDR array have been amplified by a factor of 5. 

The measurements with both arrays for Day 1 are with a Farmer detector chamber infront of the array wich is visible in the data. Only the energy and RFKO gain has been changed for every measurement. The files are named accordingly.

The measurements with the OD 1600XDR array for Day 2 contain variations of the collimators (10 cm and 5 cm), changes in magnetic optics (mag opt1 is large and mag opt2 is small) and variations of the last two quadrupole magnets (2quad).

## Additional information on the data resolution and orientation

1) The center-to-center distance between each point of the grid is 2.5 mm in both directions.

2) Yes, the array has more resolution in the center, we interpolated the data in the nan points.

3) The Farmer chamber has to be on the right side of the plot.


# Files and scripts

I've used the matlab_script_convert_to_h5.m to convert the .m files to h5 which contains the start and end date. Opening the .m straight away with python you can't decode the timestamp (matlab propriatary format).

parse_data.ipynb loops through all data and computes the beam size

octavius_data.pickle is the final output